import subprocess
import pandas as pd
import ast
from pandas.core.frame import DataFrame

### APPROACH 1 - Simple read with pandas

# Working Files & Constants
zip_loc = r'publications_min.csv.gz'
unzip_command = 'gunzip -k ' + zip_loc

# Unzip the .gz file & read raw_data
try:
    subprocess.run(unzip_command, shell=True, check=True)
except FileNotFoundError:
    print('File does not exist or was already unziped')
    pass

try:
    raw_data = pd.read_csv('publications_min.csv', index_col=0)
    print('File read successfuly')
except FileNotFoundError:
    print('File was not found, stopping script execution')
    

# Epmty lists for the loop

authors_list = []
potential_exceptions = []

# Loop creating a list of unique authors
for record, value in enumerate(raw_data.authors):
    try:
        # Because the list containing authors is actualy a string in
        # our dataframe, we can use literal_eval to convert it to a normal list.
        # To get rid of potential duplicates we concatenate lists as a set, obvious duplicates are removed.
        authors_list = list(set(authors_list + ast.literal_eval(value)))
    except Exception as e:
        # Due to the fact that we can run into error from literal_eval
        # we defined try except and log ourselves encountered errors.
        potential_exceptions.append('row: ' + str(record) + ' error: ' + str(e))
        continue

# Split the obtained list and create final dataframe
authors_split = [author.rsplit(' ', 1) for author in authors_list]
unique_people = pd.DataFrame(authors_split, columns=['firstname', 'lastname'])
unique_people.to_csv('unique_people.csv')

